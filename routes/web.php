<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about',function(){
    return view('about');
});

Route::get('/bakso/{masukkankota?}',function($kotaa=null){
    echo "Saya Makan Bakso ".$kotaa;
});

Route::get('/contact/{nama?}','ContactController@index');
Route::get('/tampil','ContactController@tampil');
Route::get('/contact/hapus/{id}','ContactController@hapus');
Route::get('/contact/edit/{id}','ContactController@edit');
Route::post('/contact/proses','ContactController@simpan');
Route::post('/contact/svedit','ContactController@simpanedit');