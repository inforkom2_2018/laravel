<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function index($namadia=null)
    {
        return view('contact',compact('namadia'));
        //return "Aku";
    }

    public function proses(Request $req)
    {
        $emailnya=$req->email;
        $namanya=$req['nama'];
        $pesannya=$req->pesan;
        return "Email anda $emailnya dengan namamu adalah $namanya sedangkan pesan anda adalah $pesannya";
    }

    public function simpan(Request $req)
    {
        $emailnya=$req->email;
        $namanya=$req->nama;
        $pesannya=$req->pesan;

        //Simpan 1 table
        $berhasil=DB::table('kontak')->insert([
            "nama"=>$namanya,
            "email"=>$emailnya,
            "pesan"=>$pesannya
        ]);


        if($berhasil){
            return redirect (url('tampil'));
        }
        else{
            echo "Gagal";
        }

    }

    public function tampil()
    {
        $hasil=(Object)["id"=>"","nama"=>"","email"=>"","pesan"=>""];
        $dataa=DB::table('kontak')->get();
        return view('tampil',compact('dataa','hasil'));
    }

    public function hapus($id)
    {
        DB::table('kontak')->where("id",$id)->delete();
        return redirect (url('tampil'));
    }

    public function edit($id)
    {
        $dataa=DB::table('kontak')->get();
        $hasil=DB::table('kontak')->where("id",$id)->first();
        return view('tampil',compact('dataa','hasil'));
    }

    public function simpanedit(Request $req)
    {
        $idnya=$req->id;
        $emailnya=$req->email;
        $namanya=$req->nama;
        $pesannya=$req->pesan;

        //Simpan 1 table
        $berhasil=DB::table('kontak')->where('id',$idnya)->update([
            "nama"=>$namanya,
            "email"=>$emailnya,
            "pesan"=>$pesannya
        ]);

        if($berhasil){
            return redirect (url('tampil'));
        }
        else{
            echo "Gagal";
        }
    }
}
