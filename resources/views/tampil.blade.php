@extends('template.ori')
@section('sidebar')
    <table class="table">
    <thead>
        <tr>
        <th scope="col">Id</th>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">Pesan</th>
        <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($dataa as $rsdt)
        <tr>
        <th scope="row">{{$rsdt->id}}</th>
        <td>{{$rsdt->nama}}</td>
        <td>{{$rsdt->email}}</td>
        <td>{{$rsdt->pesan}}</td>
        <td>
            <a href="{{url('contact/edit/'.$rsdt->id)}}">EDIT</a>  |  
            <a href="{{url('contact/hapus/'.$rsdt->id)}}">HAPUS</a>

        </td>
        </tr>
        <tr>
       @endforeach
    </tbody>
    </table>
@stop
@section('judul')
    Contactnya
@stop
@section('info')

@if($hasil->id)
    @php($url=url('contact/svedit'))
@else
    @php($url=url('contact/proses'))
@endif

    <form action="{{$url}}" method="post">
    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="hidden" class="form-control" name="id" value="{{$hasil->id}}">

        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{$hasil->email}}">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Nama</label>
        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama" name="nama" value="{{$hasil->nama}}">
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Pesan</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="pesan">{{$hasil->pesan}} </textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@stop