@include('template.header')
<body>
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">PT. SUKSES</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('about')}}">About</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('contact')}}">Contact</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown link
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </li>
        </ul>
    </div>
    </nav>

    <div class="row">
        <div class="col-md-4">
            <img src="{{asset('image/about.png')}}" alt="" width='100%'>
        </div>
        <div class="col-md-8">
            <h1>ABOUT</h1>
            <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. In accusantium dignissimos ea quos earum dicta, quod ad rerum, eaque atque placeat aut non nostrum laboriosam error expedita tempora illo ipsa.
            </p>  <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. In accusantium dignissimos ea quos earum dicta, quod ad rerum, eaque atque placeat aut non nostrum laboriosam error expedita tempora illo ipsa.
            </p>  <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. In accusantium dignissimos ea quos earum dicta, quod ad rerum, eaque atque placeat aut non nostrum laboriosam error expedita tempora illo ipsa.
            </p>  <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. In accusantium dignissimos ea quos earum dicta, quod ad rerum, eaque atque placeat aut non nostrum laboriosam error expedita tempora illo ipsa.
            </p>  <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. In accusantium dignissimos ea quos earum dicta, quod ad rerum, eaque atque placeat aut non nostrum laboriosam error expedita tempora illo ipsa.
            </p>
        </div>
    </div>
</body>
</html>