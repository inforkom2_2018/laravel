@extends('template.ori')
@section('sidebar')
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3954.5128434786566!2d111.53061981433282!3d-7.627862994500162!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e79bef1b013b46b%3A0xc7b5a3e8a713f9fa!2sWearnes+Education+Center+%26+ROYAL+OCEAN+International+Madiun!5e0!3m2!1sid!2sid!4v1553689018500" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
@stop
@section('judul')
    Contactnya {{$namadia}}
@stop
@section('info')
    <form action="{{url('contact/proses')}}" method="post">
    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Nama</label>
        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama" name="nama">
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Pesan</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="pesan"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@stop