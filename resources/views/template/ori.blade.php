@include('template/header')
@include('template.menu')

<div class="row">
    <div class="col-md-8">
        @yield('sidebar')
    </div>
    <div class="col-md-4">
        <h1>@yield('judul')</h1>
        @yield('info')
    </div>
</div>
@include('template.footer')